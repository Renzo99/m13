import javax.swing.*;
import java.util.ArrayList;

public class Caja extends Thread{

    private int iDCaja;
    private ArrayList<Cliente> listaClientes=new ArrayList<>();
    private long tiempoDeTrabajo;
    private int compra_grande;
    private int compra_mediana;
    private int compra_pequenia;
    private JTextArea txtCaja1;
    private JLabel Caja;
    private JProgressBar barra;

    public Caja(int iDCaja, JTextArea txtCaja1, JProgressBar progressBar, JLabel Caja){
        this.iDCaja = iDCaja;
        this.tiempoDeTrabajo = System.currentTimeMillis();
        this.txtCaja1 = txtCaja1;
        this.barra = progressBar;
        this.Caja = Caja;
    }

    public Caja(ArrayList<Cliente> listaClientes) {
        this.listaClientes = listaClientes;
    }

    public int getiDCaja() {
        return iDCaja;
    }

    public void setiDCaja(int iDCaja) {
        this.iDCaja = iDCaja;
    }

    public ArrayList<Cliente> getListaClientes() {
        return listaClientes;
    }

    public long getTiempoDeTrabajo() {
        return tiempoDeTrabajo;
    }

    public void setTiempoDeTrabajo(long tiempoDeTrabajo) {
        this.tiempoDeTrabajo = tiempoDeTrabajo;
    }

    public int getCompra_grande() {
        return compra_grande;
    }

    public void setCompra_grande(int compra_grande) {
        this.compra_grande = compra_grande;
    }

    public int getCompra_mediana() {
        return compra_mediana;
    }

    public int getNum_clientes_gestionado(){
        ArrayList<Cliente> clientes = getListaClientes();
        int contador=0;
        for (int i = 0; i < clientes.size(); i++) {
            contador++;
        }
        return contador;
    }

    public void setCompra_mediana(int compra_mediana) {
        this.compra_mediana = compra_mediana;
    }

    public int getCompra_pequenia() {
        return compra_pequenia;
    }

    public void setCompra_pequenia(int compra_pequenia) {
        this.compra_pequenia = compra_pequenia;
    }

    @Override
    public void run() {
        Cliente c= Mercadona.asignarCliente();
        txtCaja1.setText("");

        while( c!=null) {

            //añado el cliente a la lista de clientes de la caja
            listaClientes.add(c);

            //muestro la informacion por pantalla
            mostrarClientes(c);

            System.out.println("Caja ["+this.iDCaja + "] " +
                    "cliente que esta atendiendo [" + c.getIdCliente()+"], tiempo incial "+(System.currentTimeMillis() - this.tiempoDeTrabajo) / 1000 + "seg");

            for (int i = 0; i< c.getListasProductos().size(); i++) {
                tiempoDeEspera(c.getListasProductos().get(i));
                System.out.println("Caja [" +this.iDCaja + "] Procesado el producto " + (i + 1) + " del cliente [" + c.getIdCliente()+
                        "] ->Tiempo: " + (System.currentTimeMillis() - tiempoDeTrabajo) / 1000 + "seg");

            }
            System.out.println("Caja [" +this.iDCaja + "] HA TERMINADO CON EL CLIENTE ["
                    + c.getIdCliente() + "] EN EL TIEMPO: "
                    + (System.currentTimeMillis() - this.tiempoDeTrabajo) / 1000 + "seg");

            c= Mercadona.asignarCliente();
            actualizarBarra(c);

        }
        setTiempoDeTrabajo((System.currentTimeMillis() - this.tiempoDeTrabajo) / 1000);
        Caja.setText(" Tiempo = "+getTiempoDeTrabajo()+" s");
    }

    private synchronized void actualizarBarra(Cliente c){
        if(c!=null){
            double valor =(float) Mercadona.clientesF*100/Mercadona.tot_clientes;
            barra.setValue((int) Math.round(valor));
        }

    }
    private void mostrarClientes(Cliente c) {

        txtCaja1.append("cliente numero: " + c.getIdCliente() + "\n");
    }

    private void tiempoDeEspera(Integer segundos) {
        try {
            Thread.sleep(segundos * 1000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

    public void compras(){
        int contador_grande=0;
        int contador_mediana=0;
        int contador_pequenia=0;
        ArrayList<Cliente> clientes = getListaClientes();
        for (int i = 0; i < clientes.size(); i++) {
            Cliente c= clientes.get(i);
            ArrayList<Integer> productos = c.getListasProductos();
            for (int j = 0; j <productos.size() ; j++) {
                int p= productos.get(j);
                switch (p) {
                    case 1 -> contador_pequenia++;
                    case 3 -> contador_mediana++;
                    case 6 -> contador_grande++;

                }
            }
        }
        setCompra_grande(contador_grande);
        setCompra_pequenia(contador_pequenia);
        setCompra_mediana(contador_mediana);
    }

    @Override
    public String toString() {
        return "Caja{" +
                "iDCaja=" + iDCaja +
                ", listaClientes=" + listaClientes +
                ", tiempoDeTrabajo=" + tiempoDeTrabajo +
                ", compra_grande=" + compra_grande +
                ", compra_mediana=" + compra_mediana +
                ", compra_pequenia=" + compra_pequenia +
                '}';
    }
}
