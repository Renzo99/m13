import com.mongodb.*;
import com.mongodb.MongoClient;
import com.mongodb.client.*;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Coneccion {

  public static void insertarDatos(ArrayList<Caja>cajas_list){

        MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://localhost:27017"));

        MongoDatabase database = mongoClient.getDatabase("M13");
        MongoCollection<Document> cajas = database.getCollection("Mercadona");
        Document Caja = new Document("_id", new ObjectId());
        ArrayList<Document> documentos = new ArrayList<>();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date date = new Date();
        for (int i = 0; i < cajas_list.size(); i++) {
            Caja = new Document("_id", new ObjectId());
            Caja.append("Dia",String.valueOf(dateFormat.format(date))).append("Id_caja",cajas_list.get(i).getiDCaja())
                    .append("Clientes_gestionados",cajas_list.get(i).getNum_clientes_gestionado())
                    .append("Tiempo_total",Long.valueOf(cajas_list.get(i).getTiempoDeTrabajo()))
                    .append("Productos_grandes",cajas_list.get(i).getCompra_grande())
                    .append("Productos_medianos",cajas_list.get(i).getCompra_mediana())
                    .append("Productos_pequeños",cajas_list.get(i).getCompra_pequenia());
            cajas.insertOne(Caja);
        }

        System.out.println("Datos insertados");
    }

    public static ArrayList<String> GetCajaConMenosTiempo() {

        MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://localhost:27017"));
        ArrayList<String> resultados= new ArrayList<>();

        DB database = mongoClient.getDB("M13");

        DBCollection collection = database.getCollection("Mercadona");
        DBCursor cursor = collection.find().sort(new BasicDBObject("Tiempo_total",-1));
        Iterator<DBObject> cajas = cursor.iterator();
        while(cajas.hasNext()) {
            DBObject caja=cajas.next();
            String r= "Id_Caja: "+caja.get("Id_caja")+" tiempo_total: "+caja.get("Tiempo_total")+ " dia: "+caja.get("Dia");
            resultados.add(r);
        }
        return resultados;

    }
    public static ArrayList<String> GetCajaConMasClientes() {

        MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://localhost:27017"));
        ArrayList<String> resultados= new ArrayList<>();

        DB database = mongoClient.getDB("M13");

        DBCollection collection = database.getCollection("Mercadona");
        DBCursor cursor = collection.find().sort(new BasicDBObject("Clientes_gestionados",1));
        Iterator<DBObject> cajas = cursor.iterator();
        while(cajas.hasNext()) {
            DBObject caja=cajas.next();
            String r="Id_Caja: "+caja.get("Id_caja")+" Clientes: "+caja.get("Clientes_gestionados")+ " dia: "+caja.get("Dia");
            resultados.add(r);
        }
        return resultados;
    }
    public static ArrayList<String> GetCajaConMasProductosGrandes() {

        MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://localhost:27017"));
        ArrayList<String> resultados= new ArrayList<>();

        DB database = mongoClient.getDB("M13");

        DBCollection collection = database.getCollection("Mercadona");
        DBCursor cursor = collection.find().sort(new BasicDBObject("Productos_grandes",1));
        Iterator<DBObject> cajas = cursor.iterator();
        while(cajas.hasNext()) {
            DBObject caja=cajas.next();
            String r="Id_Caja: "+caja.get("Id_caja")+" Productos grandes: "+caja.get("Productos_grandes")+ " dia: "+caja.get("Dia");
            resultados.add(r);
        }
        return resultados;

    }
    public static ArrayList<String> GetCajaConMasProductosMedianos() {

        MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://localhost:27017"));
        ArrayList<String> resultados= new ArrayList<>();

        DB database = mongoClient.getDB("M13");

        DBCollection collection = database.getCollection("Mercadona");
        DBCursor cursor = collection.find().sort(new BasicDBObject("Productos_medianos",1));
        Iterator<DBObject> cajas = cursor.iterator();
        while(cajas.hasNext()) {
            DBObject caja=cajas.next();
            String r="Id_Caja: "+caja.get("Id_caja")+" Productos medianos: "+caja.get("Productos_medianos")+ " dia: "+caja.get("Dia");
            resultados.add(r);
        }
        return resultados;

    }
    public static ArrayList<String> GetCajaConMasProductosPequenios() {

        MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://localhost:27017"));
        ArrayList<String> resultados= new ArrayList<>();
        DB database = mongoClient.getDB("M13");

        DBCollection collection = database.getCollection("Mercadona");
        DBCursor cursor = collection.find().sort(new BasicDBObject("Productos_pequeños",1));
        Iterator<DBObject> cajas = cursor.iterator();
        while(cajas.hasNext()) {
            DBObject caja=cajas.next();
            String r= "Id_Caja: "+caja.get("Id_caja")+" Productos pequeños: "+caja.get("Productos_pequeños")+ " dia: "+caja.get("Dia");
            resultados.add(r);
        }
        return resultados;

    }
    }

