import java.util.ArrayList;

public class Cliente {

    private int IdCliente;
    private ArrayList<Integer> listasProductos = new ArrayList<>();

    public Cliente(int idCliente, ArrayList<Integer> listasProductos) {
        IdCliente = idCliente;
        this.listasProductos = listasProductos;
    }

    public Cliente(int idCliente) {
        IdCliente = idCliente;
    }

    public void setIdCliente(int idCliente) {
        IdCliente = idCliente;
    }

    public int getIdCliente() {
        return IdCliente;
    }

    public ArrayList<Integer> getListasProductos() {
        return listasProductos;
    }

    public void setListasProductos(ArrayList<Integer> listasProductos) {
        this.listasProductos = listasProductos;
    }

    public void rellenarCompra(){
        int tot_productos = Metodos.generaNumeroAleatorio(1, 20);
        for (int i = 0; i <tot_productos ; i++) {
            int tipo_producto = Metodos.generaNumeroAleatorio(1, 4);
            switch (tipo_producto) {
                case 1 -> this.listasProductos.add(1);
                case 2 -> this.listasProductos.add(3);
                case 3 -> this.listasProductos.add(6);
            }

        }
    }

    @Override
    public String toString() {
        return "Testing.Pruebas.Cliente{" +
                "IdCliente=" + IdCliente +
                ", listasProductos=" + listasProductos +
                '}';
    }
}
