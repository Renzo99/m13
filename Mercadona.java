
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Mercadona extends JFrame {
    private JPanel panel1;
    private JButton comenzar;
    private JProgressBar progressBarClientes;
    private JTextArea Caja1;
    private JTextArea Caja2;
    private JTextArea Caja3;
    private JTextArea Caja4;
    private JLabel titleCaja1;
    private JLabel titleCaja2;
    private JLabel titleCaja3;
    private JLabel titleCaja4;
    private JLabel totalClientes;
    private JLabel titleProgreso;
    private JButton reinicio;
    private JLabel tiempo1;
    private JLabel tiempo2;
    private JLabel tiempo4;
    private JLabel tiempo3;
    private JLabel Progreso;
    private JLabel porcentaje;
    public static int tot_clientes=0;
    public static int clientesF=0;
    private static ArrayList<Cliente> listaClientes;
    private SwingWorker<Void,String> worker;
    public static ArrayList<Caja> cajaArrayList= new ArrayList<>();

    public JProgressBar getProgressBarClientes() {
        return progressBarClientes;
    }


    public JTextArea getCaja1() {
        return Caja1;
    }

    public JTextArea getCaja2() {
        return Caja2;
    }

    public JTextArea getCaja3() {
        return Caja3;
    }


    public JTextArea getCaja4() {
        return Caja4;
    }




    public Mercadona() throws InterruptedException {
        super();
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setContentPane(panel1);
        this.setBounds(500, 300, 870, 500);
        this.pack();
        tiempo1.setVisible(false);
        tiempo2.setVisible(false);
        tiempo3.setVisible(false);
        tiempo4.setVisible(false);
        comenzar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                progressBarClientes.setValue(0);
                progressBarClientes.repaint();
                totalClientes.setText("Total Clientes = ");
                inicio();
            }
        });
    }

    private void inicio(){

        worker = new SwingWorker<Void, String>() {
            @Override
            protected Void doInBackground() throws Exception {
                rellenarClientes();
                iniciar();
                return null;
            }
        };
        worker.execute();

    }


    public void rellenarClientes(){
        listaClientes = new ArrayList<>();

        tot_clientes = Metodos.generaNumeroAleatorio(4, 6); //genero un numero aleatorio de clientes
        System.out.println("tot clientes: "+ tot_clientes);
        totalClientes.setText(totalClientes.getText()+" "+String.valueOf(tot_clientes));
        for (int i = 1; i <=tot_clientes ; i++) {
            Cliente cliente = new Cliente(i);
            cliente.rellenarCompra(); //relleno el cliente con la compra
            listaClientes.add(cliente); //añado el cliente al arraylist de clientes
        }
    }

    public static synchronized Cliente asignarCliente(){

        if(listaClientes.size() != 0) {
            Cliente c= listaClientes.get(0);
            listaClientes.remove(0);
            clientesF ++;
            return c;
        }else {
            return null;
        }
    }


    public void iniciar() throws InterruptedException {

        Caja h1 = new Caja(1,Caja1,progressBarClientes,tiempo1);
        Caja h2 = new Caja(2,Caja2,progressBarClientes,tiempo2);
        Caja h3 = new Caja(3,Caja3,progressBarClientes,tiempo3);
        Caja h4 = new Caja(4,Caja4,progressBarClientes,tiempo4);
        h1.start();
        h2.start();
        h3.start();
        h4.start();

        h1.join();
        h2.join();
        h3.join();
        h4.join();


        tiempo1.setVisible(true);
        tiempo2.setVisible(true);
        tiempo3.setVisible(true);
        tiempo4.setVisible(true);

        h1.compras();
        h2.compras();
        h3.compras();
        h4.compras();

        cajaArrayList.add(h1);
        cajaArrayList.add(h2);
        cajaArrayList.add(h3);
        cajaArrayList.add(h4);

        Coneccion.insertarDatos(cajaArrayList);

    }


}
