
import java.util.Random;

public class Metodos {

    /**
     * Genera un numero aleatorio entre un minimo y un maximo
     * @param minimo
     * @param maximo
     * @return
     */
    public static int generaNumeroAleatorio(int minimo, int maximo){
        Random r = new Random();
        return r.nextInt(maximo-minimo) + minimo;
    }

}
